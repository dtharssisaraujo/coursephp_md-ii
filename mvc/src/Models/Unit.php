<?php
    namespace MVC\Models;
    use MVC\Core\Model;

    class Unit extends Model
    {
        public function getAll()
        {
            $pdo = $this->mapper;
            $res = $pdo->query('SELECT * FROM unit');
            return $res->fetchAll(\PDO::FETCH_OBJ);
        }
        public function getById($id)
        {
            $pdo = $this->mapper;
            $res = $pdo->query('SELECT * FROM unit WHERE id='. $id);
            return $res->fetchAll(\PDO::FETCH_OBJ);
        }
    }