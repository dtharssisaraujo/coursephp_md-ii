<?php

namespace MVC\Controllers;
use MVC\Core\Controller;
use MVC\Core\View;
use MVC\Models\Unit;

class SiteController extends Controller
{
    public function indexAction($nome)
    {
        View::template('site/index');
    }

    public function unitsAction()
    {
        $unit = new Unit;
        $units = $unit->getAll();
        View::template('site/unit', [
            'units' => $units
        ]);

        //var_dump($unit->getAll());
    }

    public function unitAction($id)
    {
        $unit = new Unit;
        $singleUnit = $unit->getById($id);
        if ($singleUnit){
            View::template('site/unit', [
                'units' => $singleUnit
            ]);
        }else{
            View::template('site/unitnotfound');
        }
    }

    public function helloAction($nome='zé ninguém')
    {
        View::template('site/hello', ['nome' => $nome]);
    }
    
    public function testeAction()
    {
        View::template('site/index');
    }
    
    public function templateAction()
    {
        View::render('templateFull');
    }
    
    public function testAction()
    {
        $robot = new \MVC\Models\Robot();
        $robot->get(1);
        var_dump($robot);
    }
}

 
