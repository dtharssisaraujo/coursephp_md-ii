<?php

$noticias = [
    [
        'titilo' => 'Alunos da Impacta que dormem na sala',
        'desc' => 'Alunos não dormem a noite, chega na aula comem e dormem',
        'link' => 'http://impacta.com.br/sleep'
    ],
    [
        'titilo' => 'Alunos da Impacta que comem no Mac',
        'desc' => 'Alunos que enche a barriga',
        'link' => 'http://impacta.com.br'
    ],
    [
        'titilo' => 'fogo na caixa da agua',
        'desc' => 'fogo na caixa d agua',
        'link' => 'http://impacta.com.br'
    ]
];

$xml = new SimpleXMLElement('<?mxl version="1.0" charset="utf-8"?><rss version="2.0"></rss>');

$channel = $xml->addChild('channel');

$channel -> addChild('title', 'Noticias Impacta');
$channel -> addChild('description', 'Noticias Impacta - As melhores novidades the World for you');

foreach($noticias as $noticia){
    $item = $channel->addChild('item');
    $item->addChild('title', $noticia['titulo']);
    $item->addChild('link', $noticia['link']);
    $item->addChild('description', $noticia['desc']);
}

echo $xml->asXML('rss.xml   ');