<?php
abstract class Registro
{
    public $id;
    public $nome;
    public $tabela;

        abstract public function setTabela();
}

class Cliente extends Registro
{
    public function setTabela()
    {
        $this->tabela = 'ai_tbl_clientes';
    }
}
class Produto extends Registro
{
    public function setTabela()
    {
        $this->tabela = 'tbl_produtos';
    }
}
