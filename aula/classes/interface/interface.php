<?php
interface bancoGenerico
{
	public function depositar($valor);		
	public function sacar($valor);		
	public function extratoEmTela();			
}



class ItauATM implements bancoGenerico
{
	public function depositar($valor)
	{
		echo 'Deposita o valor ' . $valor;
	}
	public function sacar($valor)
	{
		echo 'Saca o valor ' . $valor;
	}
	public function extratoEmTela()
	{
		echo 'Veja seu Extrato ';
	}
}




class SantanderATM implements bancoGenerico
{
	public function depositar($valor)
	{
		echo 'Santander: Deposita o valor ' . $valor;
	}
	public function sacar($valor)
	{
		echo 'Santander: Saca o valor ' . $valor;
	}
	public function extratoEmTela()
	{
		echo 'Santander: Veja seu Extrato ';
	}
}


