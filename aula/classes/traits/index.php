<?php
trait Registro
{
    public function save()
    {
        echo 'grava no SESSION';
    }
}
trait mongo
{
    public function save()
    {
        echo 'grava no mongo';
    }
}

trait Database
{
    public function save()
    {
        echo   'grava no banco';
    }
}
class Cliente
{
    use Database, Registro, mongo{
        Registro::save insteadof Monginho, Database;
        Database::save as saveDb;
        mongo::save as saveMonginho;
    }
}

$clientes = new Cliente;
$clientes->save();
$clientes->saveDb();
$clientes->saveMonginho();
