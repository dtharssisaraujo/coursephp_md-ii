<?php

class ClientesGold implements PlanosInterface
{
    public function desconto($valor)
    {
        return $valor - ($valor * 0.25);
    }
    public function taxa()
    {
        return 100;
    }
}

class ClientesSilver implements PlanosInterface
{
    public function desconto($valor)
    {
        return $valor - ($valor * 0.15);
    }
    public function taxa()
    {
        return 50;
    }
}
class ClientesBronze implements PlanosInterface
{
    public function desconto($valor)
    {
        return $valor - ($valor * 0.05);
    }
    public function taxa()
    {
        return 20;
    }
}