<?php
require 'PlanosInterface.php';
require 'Clientes.php';
require 'PlanoStrategy.php';

$strategy = new PlanoStrategy(new ClientesGold);

echo 'Total da Compra de 100: ';
echo $strategy->getTotal(100) . '<br>';

echo 'Taxa ao Mês: ';
echo $strategy->getTaxa();