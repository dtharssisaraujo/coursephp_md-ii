<?php

interface PlanosInterface
{
    public function desconto($valor);
    public function taxa();
}