
<?php

class PlanoStrategy
{
    protected $tipoCli;

    public function  __construct(PlanosInterface $tipo)
    {
        $this->tipoCli = $tipo;
    }
    public function getTotal($preco)
    {
        return $this->tipoCli->desconto($preco);
    }
    public function getTaxa()
    {
        return $this->tipoCli->taxa();
    }
}