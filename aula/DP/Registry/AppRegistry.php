<?php

class AppRegistry
{
    private static $instances;

    public function register($nome, $obj)
    {
        self::$instances[] = $obj;
    }

    public function get($nome)
    {
        return self::$instances[$nome];
    }
}