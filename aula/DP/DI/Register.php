<?php

interface Register
{
    public function save();
}

class DbRegister implements Register
{
    public function save()
    {
        echo 'salva no banco';
    }
}

class MongoRegister implements Register
{
    public function save()
    {
        echo 'salva no mongoDb';
    }
}