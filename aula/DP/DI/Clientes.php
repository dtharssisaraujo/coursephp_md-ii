<?php

class Cliente
{
    public $nome;
    public $register;

    public function setRegister(Register $reg)
    {
        $this->register = $reg;
    }

    public function gravar()
    {
        $this->register->save();
    }
}