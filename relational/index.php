<?php
    require __DIR__ . '/vendor/autoload.php';

    use Respect\Relational\Db;

    $pdo = new PDO ('mysql:dbname=mysql', 'root', '');

    $pdo->query('CREATE TABLE unit (testez INT PRIMARY KEY AUTO_INCREMENT, testa INT, testb VARCHAR(255))');
    $pdo->query("INSERT INTO unit(testa, testb) VALUES (10, 'abc')");
    $pdo->query("INSERT INTO unit(testa, testb) VALUES (20, 'def')");
    $pdo->query("INSERT INTO unit(testa, testb) VALUES (30, 'ghi')");

